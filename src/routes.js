import React from 'react';
import { BrowserRouter, Switch, Route} from 'react-router-dom';
import Header from './componentes/Header';

import Main from './pages/main/index';
import Home from './pages/main/home'
import Product from './pages/products';
import ClienteCadastro from './pages/cliente/cadastro'

import SushiHome from './SushiHome.png';
import "./styles.css";

const Routes = () => (
    <BrowserRouter>
        <Header />
        <img className="imgHome" src={SushiHome} alt="home"></img>
        <Switch>
            <Route exact path="/" component={Home}/>
            <Route exact path="/cliente" component={Main}/>
            <Route path="/cliente/cadastro" component={ClienteCadastro}/>
            <Route path="/cliente/:id" component={Product}/>
        </Switch>
    </BrowserRouter>
)

export default Routes;