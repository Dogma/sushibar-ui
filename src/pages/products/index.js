import React, {Component} from 'react';
import api from '../../services/api';
import { Link } from 'react-router-dom';
import './styles.css';

export default class Product extends Component {

    state = {
        cliente: {
            endereco: {
                cidade: {
                    estado: {

                    }
                }
            }
        }
    };

    async componentDidMount() {
        const { id } = this.props.match.params;
        const response = await api.get(`/cliente/${id}`);
        this.setState({ cliente: response.data});
    }

    render() {
        const cliente = this.state.cliente;
        return (
            <div className="product-info">
                <h1>{cliente.nome}</h1>
                <p>{cliente.apelido}</p>
                <p>{cliente.email}</p>
                <h2>Endereço</h2>
                <div className="actions">
                <p>{cliente.endereco.cep} {cliente.endereco.logradouro} {cliente.endereco.numero}</p>
                <p>{cliente.endereco.cidade.nome} - {cliente.endereco.cidade.estado.nome}</p>
                    <Link to="/cliente">Anterior</Link>
                </div>                
            </div>
        );
    }
}