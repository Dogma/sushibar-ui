import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import api from '../../../services/api';
import './style.css';

export default class cadastroCliente extends Component {

    constructor() {
        super();
        this.state = {
            nome: "",
            apelido: "",
            email: "",
            senha: "",
            ativo: true,
            endereco: {
                cep: "",
                logradouro: "",
                numero: "",
                complemento: "",
                bairro: "",
                cidade: {
                    nome: ""
                }
            }
        };
    }

    onChange = (event) => {
        const state = Object.assign({}, this.state);
        const campo = event.target.name;
        const value = event.target.value;
        state[campo] = value;
        this.setState(state);
    }

    onChangeEndereco = (event) => {
        const state = Object.assign({}, this.state);
        const campo = event.target.name;
        const value = event.target.value;
        state.endereco[campo] = value;
        this.setState(state);
    }

    onChangeEnderecoCidade = (event) => {
        const state = Object.assign({}, this.state);
        const campo = event.target.name;
        const value = event.target.value;
        state.endereco.cidade[campo] = value;
        this.setState(state);
    }

    enviar = () => {
        api.post('/cliente', this.state)
        .then(function(response){
            alert(`Clietne ${this.state.nome} Salvo`);
        }); 
    }

    render() {
        return (
            <div>
                <div className="form-info">
                <h1>Informações pessoais</h1>
                <label>Nome:</label> <input name="nome" type="text"
                onChange={this.onChange} value={this.state.nome} /><br />
                <label>Apelido:</label> <input name="apelido" type="text"
                onChange={this.onChange} value={this.state.apelido}/><br />
                <label>Email:</label> <input name="email" type="text"
                onChange={this.onChange} value={this.state.email}/><br />
                <label>Senha:</label> <input name="senha" type="password"
                onChange={this.onChange} value={this.state.senha}/>
                </div>
                <div className="form-info">
                <h1>Endereço pessoal</h1>
                <label>CEP:</label> <input name="cep" type="text"
                onChange={this.onChangeEndereco} value={this.state.endereco.cep} /><br />
                <label>Logradouro:</label> <input name="logradouro" type="text"
                onChange={this.onChangeEndereco} value={this.state.endereco.logradouro} /><br />
                <label>Numero:</label> <input name="numero" type="text"
                onChange={this.onChangeEndereco} value={this.state.endereco.numero}/><br />
                <label>Complemento:</label> <input name="complemento" type="text"
                onChange={this.onChangeEndereco} value={this.state.endereco.complemento}/><br />
                <label>Bairro:</label> <input name="bairro" type="text"
                onChange={this.onChangeEndereco} value={this.state.endereco.bairro}/><br />
                <label>Cidade:</label> <input name="nome" type="text"
                onChange={this.onChangeEnderecoCidade} value={this.state.endereco.cidade.nome}/>
                <Link onClick={this.enviar} to="/">Cadastrar</Link>
                </div>
            </div>
        )
    }
}