import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Routes from '../../routes'
import api from '../../services/api';
import './styles.css';

export default class Main extends Component {
    state = {
        cliente: [],
    };

    componentDidMount() {
        this.loadcliente();
    };

    loadcliente = async (page = 1) => {
        const response = await api.get('/cliente');
        const {data} = response;
        this.setState({cliente: data});
    };

    remover = (event) => {
        const id = event.target.value;
        console.log(id);
        api.delete(`/cliente/${id}`)
        .then(() => {
        });
    }

    render() {
        const { cliente } = this.state;
        return (
            <div className="cliente-list">
                {cliente ? cliente.map(cliente => (
                    <article key={cliente.id}>
                        <p>Nome: {cliente.nome}</p>
                        <p>Apelido: {cliente.apelido}</p>
                        <p>email: {cliente.email}</p>
                        <Link to={`/cliente/${cliente.id}`}>Acessar</Link>
                        <button value={cliente.id} onClick={this.remover}>Remover</button>
                    </article>
                )):
                <div>
                    <h1>Não há clientes cadatrados</h1>
                </div>}
            </div>
        )
    };
}