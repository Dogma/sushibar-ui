import React from 'react';
import logo from './sushi-logo.png';
import { Link } from 'react-router-dom';

import "./styles.css";

const Header = () => (
    <header className="main-header">
    <a className='logo' href="/">
    <img src={logo} alt="logo"/> <br></br>Sushibar</a>
    <a href="/">Principal</a>
    <a href="/">Cardápio</a>
    <Link to="/cliente">Clientes</Link>
    <Link to="/cliente/cadastro">Cadastar</Link>
    <a href="/">Contato</a>
    </header>
);

export default Header;